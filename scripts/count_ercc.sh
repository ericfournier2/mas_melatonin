module load samtools

for i in output/ercc_align/*.sorted.bam
do
    echo $i
    samtools view -c $i
done
