for i in output/pipeline/trim/*/*.pair1.fastq.gz
do
    R1=$i
    R2=`echo $R1 | sed -e 's/pair1/pair2/'`
    fullpath=`dirname $i`
    sample=`basename $fullpath`
        
    jobpath=output/jobs/ercc_align
    mkdir -p $jobpath
    script=$jobpath/$sample.sh
    
    outpath=output/ercc_align
    mkdir -p $outpath
    
    cat << EOF > $script
#!/bin/bash
module load mugqic/star/2.7.2b
module load samtools

STAR --runMode alignReads \
  --genomeDir output/ERCC_index \
  --readFilesIn \
    $R1 \
    $R2 \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMtype BAM Unsorted \
  --outFileNamePrefix $outpath/$sample.bam \
  --limitGenomeGenerateRAM 100000000000 \
  --limitIObufferSize 4000000000     

samtools sort $outpath/$sample.bam > $outpath/$sample.bam.sorted.bam
samtools index $outpath/$sample.sorted.bam
EOF

    sbatch -D `pwd` -e $script.stderr -o $script.stdout --time 1:00:00 --cpus-per-task 16 --mem 128G --account def-masirard $script
done
