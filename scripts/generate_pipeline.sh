export RAP_ID=def-masirard
module load mugqic/genpipes/3.1.4

PROJECT_BASE=/scratch/efournie/mas_melatonin
OUTDIR=$PROJECT_BASE/output/pipeline

mkdir -p $OUTDIR
$MUGQIC_PIPELINES_HOME/pipelines/rnaseq/rnaseq.py -s '1-22' -j slurm -t cufflinks -l debug \
    -r $PROJECT_BASE/raw/readset.txt \
    -d $PROJECT_BASE/raw/design.txt \
    -o $OUTDIR \
    --config $MUGQIC_PIPELINES_HOME/pipelines/rnaseq/rnaseq.base.ini \
        $MUGQIC_PIPELINES_HOME/pipelines/rnaseq/rnaseq.graham.ini \
        $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini