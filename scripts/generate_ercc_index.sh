#!/bin/bash
#SBATCH --mem 8G
#SBATCH --cpus-per-task 1
#SBATCH --time 0:10:00
#SBATCH --account def-masirard

module load mugqic/star

mkdir -p output/ERCC_index
STAR --runMode genomeGenerate --genomeDir output/ERCC_index \
     --genomeSAindexNbases 11 --genomeFastaFiles input/ERCC92.fa
     